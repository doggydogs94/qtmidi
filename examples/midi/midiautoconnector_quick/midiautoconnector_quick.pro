TEMPLATE = app
QT += quick qml midi
CONFIG += c++11

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
    main.cpp

RESOURCES += \
    qml.qrc

target.path = $$[QT_INSTALL_EXAMPLES]/midi/midiautoconnector_quick
INSTALLS += target

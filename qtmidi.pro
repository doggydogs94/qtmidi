requires(qtHaveModule(gui))

#load(configure)
#qtCompileTest(openal)
#win32 {
#} else:mac {
#} else:qnx {
#} else:!android {
#    contains(QT_CONFIG, alsa):qtCompileTest(alsa)
#    contains(QT_CONFIG, pulseaudio):qtCompileTest(pulseaudio)
#}

OTHER_FILES += \
    sync.profile \
    .qmake.conf \
    configure.json

load(qt_parts)

#ifndef QTMIDIDEFS_P_H
#define QTMIDIDEFS_P_H

#include <QtCore/qglobal.h>

QT_BEGIN_NAMESPACE

#ifndef QT_STATIC
#    if defined(QT_BUILD_MIDI_LIB)
#        define Q_MIDI_EXPORT Q_DECL_EXPORT
#    else
#        define Q_MIDI_EXPORT Q_DECL_IMPORT
#    endif
#else
#    define Q_MIDI_EXPORT
#endif

QT_END_NAMESPACE

#endif // QTMIDIDEFS_P_H

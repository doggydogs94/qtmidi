#ifndef QMIDIPLUGINLOADER_H
#define QMIDIPLUGINLOADER_H

#include <qtmididefs.h>
#include <QtCore/qobject.h>
#include <QtCore/qstring.h>
#include <QtCore/qstringlist.h>
#include <QtCore/qmap.h>
#include <QtCore/qjsonobject.h>

QT_BEGIN_NAMESPACE

class QFactoryLoader;
class QMidiServiceProviderPlugin;

class Q_MIDI_EXPORT QMidiPluginLoader : QObject
{
public:
    QMidiPluginLoader(const char *iid,
                      const QString& suffix = QString(),
                      Qt::CaseSensitivity caseSensitivity= Qt::CaseSensitive);
    ~QMidiPluginLoader();

    QStringList keys() const;
    QObject* instance(QString const &key);
    QList<QObject*> instances(QString const &key);

private:
    void loadMetadata();

    QByteArray m_iid;
    QString m_location;
    QMap<QString, QList<QJsonObject> > m_metadata;

    QFactoryLoader *m_factoryLoader;
};

QT_END_NAMESPACE

#endif // QMIDIPLUGINLOADER_H

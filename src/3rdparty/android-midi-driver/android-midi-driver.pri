load(qt_build_paths)
CONFIG += java

ANDROID_PACKAGE_SOURCE_DIR = $$PWD

JAVACLASSPATH += $$PWD/src

JAVASOURCES += \
    $$PWD/src/jp/kshoji/driver/midi/activity/AbstractMultipleMidiActivity.java \
    $$PWD/src/jp/kshoji/driver/midi/activity/AbstractSingleMidiActivity.java \
    $$PWD/src/jp/kshoji/driver/midi/activity/MidiFragmentHostActivity.java \
    $$PWD/src/jp/kshoji/driver/midi/fragment/AbstractMidiFragment.java \
    $$PWD/src/jp/kshoji/driver/midi/util/UsbMidiDeviceUtils.java \
    $$PWD/src/jp/kshoji/driver/midi/util/UsbMidiDriver.java \
    $$PWD/src/jp/kshoji/driver/midi/util/Constants.java \
    $$PWD/src/jp/kshoji/driver/midi/util/ReusableByteArrayOutputStream.java \
    $$PWD/src/jp/kshoji/driver/midi/listener/OnMidiDeviceAttachedListener.java \
    $$PWD/src/jp/kshoji/driver/midi/listener/OnMidiDeviceDetachedListener.java \
    $$PWD/src/jp/kshoji/driver/midi/listener/OnMidiInputEventListener.java \
    $$PWD/src/jp/kshoji/driver/midi/device/MidiInputDevice.java \
    $$PWD/src/jp/kshoji/driver/midi/device/MidiDeviceConnectionWatcher.java \
    $$PWD/src/jp/kshoji/driver/midi/device/MidiOutputDevice.java \
    $$PWD/src/jp/kshoji/driver/midi/service/SingleMidiService.java \
    $$PWD/src/jp/kshoji/driver/midi/service/MultipleMidiService.java \
    $$PWD/src/jp/kshoji/driver/usb/util/DeviceFilter.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/MetaEventListener.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/UsbMidiSystem.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/Patch.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/MidiFileFormat.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/impl/SequencerImpl.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/impl/MidiChannelImpl.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/Instrument.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/Sequence.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/MidiDeviceReceiver.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/usb/UsbMidiDevice.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/usb/UsbMidiSynthesizer.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/usb/UsbMidiReceiver.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/usb/UsbMidiTransmitter.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/InvalidMidiDataException.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/MidiMessage.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/SysexMessage.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/io/StandardMidiFileWriter.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/io/StandardMidiFileReader.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/Soundbank.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/Synthesizer.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/Transmitter.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/SoundbankResource.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/MidiSystem.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/MidiEvent.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/spi/MidiFileReader.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/spi/MidiFileWriter.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/spi/SoundbankReader.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/spi/MidiDeviceProvider.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/Receiver.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/MidiDevice.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/ShortMessage.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/VoiceStatus.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/MidiDeviceTransmitter.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/MidiUnavailableException.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/Track.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/MidiChannel.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/ControllerEventListener.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/Sequencer.java \
    $$PWD/src/jp/kshoji/javax/sound/midi/MetaMessage.java \

target.path = $$[QT_INSTALL_PREFIX]/jar
INSTALLS += target

OTHER_FILES += $$JAVASOURCES

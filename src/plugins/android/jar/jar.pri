load(qt_build_paths)
CONFIG += java
DESTDIR = $$MODULE_BASE_OUTDIR/jar

JAVACLASSPATH += \
    $$PWD/src \
    $$shadowed($$PWD/../../../3rdparty/android-midi-driver/QtAndroidMidiDriver-bundled.jar)



JAVASOURCES += $$PWD/src/org/qtproject/qt5/android/midi/QAndroidMidiInterface.java

# install
target.path = $$[QT_INSTALL_PREFIX]/jar
INSTALLS += target

OTHER_FILES += $$JAVASOURCES

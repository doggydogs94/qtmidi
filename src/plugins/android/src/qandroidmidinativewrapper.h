#ifndef QANDROIDMIDINATIVEWRAPPER_H
#define QANDROIDMIDINATIVEWRAPPER_H

#include <QtMidi/qmidi.h>
#include <QtMidi/qmidisystem.h>
#include <QAndroidJniObject>
#include <QVector>
#include <QStringList>

QT_BEGIN_NAMESPACE

bool android_registerNatives();

void initAndroidManagerJNI(QAndroidJniObject *usbmanager);
void releaseAndroidManagerJNI(QAndroidJniObject usbmanager);

QStringList android_listAvailableInputDevices(QAndroidJniObject usbmanager);
QStringList android_listAvailableOutputDevices(QAndroidJniObject usbmanager);

bool android_createInputDevice(const QString &id, QAndroidJniObject usbmanager);
bool android_createOutputDevice(const QString &id, QAndroidJniObject usbmanager);
bool android_deleteInputDevice(const QString &id, QAndroidJniObject usbmanager);
bool android_deleteOutputDevice(const QString &id, QAndroidJniObject usbmanager);
void android_sendNativeMessage(const QString &id, QAndroidJniObject usbmanager, const QMidiMessage &m);


QT_END_NAMESPACE

#endif // QANDROIDMIDINATIVEWRAPPER_H

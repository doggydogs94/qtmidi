TARGET = qtandroid_midi

QT += midi-private
qtHaveModule(QT): QT += androidextras

HEADERS += \
    qandroidmidiplugin.h \
    qandroidmidinativewrapper.h \
    qandroidmidiinput.h \
    qandroidmidioutput.h

SOURCES += \
    qandroidmidiplugin.cpp \
    qandroidmidinativewrapper.cpp \
    qandroidmidiinput.cpp \
    qandroidmidioutput.cpp

OTHER_FILES += android.json

PLUGIN_TYPE = midi
PLUGIN_CLASS_NAME = QAndroidPlugin
load(qt_plugin)

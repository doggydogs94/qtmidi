#ifndef QANDROIDMIDIOUTPUT_H
#define QANDROIDMIDIOUTPUT_H

#include <QtCore/qdebug.h>

#include <QtMidi/qmidi.h>
#include <QtMidi/qmidideviceinfo.h>
#include <QtMidi/qmidisystem.h>

#include <QAndroidJniObject>

QT_BEGIN_NAMESPACE

class QAndroidMidiOutput : public QAbstractMidiOutput
{
    Q_OBJECT
public:
    QAndroidMidiOutput(const QMidiDeviceInfo &info, QAndroidJniObject usbDriver);
    virtual ~QAndroidMidiOutput();

    QMidi::Error error() const override {return QMidi::NoError;}
    QMidi::State state() const override {return QMidi::ConnectedState;}

    void sendMidiMessage(const QMidiMessage& m) const override;

private:
    QAndroidJniObject mUsbDriver;
};

QT_END_NAMESPACE

#endif // QANDROIDMIDIOUTPUT_H

#ifndef QANDROIDMIDIINPUT_H
#define QANDROIDMIDIINPUT_H

#include <QtCore/qdebug.h>

#include <QtMidi/qmidi.h>
#include <QtMidi/qmidideviceinfo.h>
#include <QtMidi/qmidisystem.h>

#include <QAndroidJniObject>

QT_BEGIN_NAMESPACE

class QAndroidMidiInput : public QAbstractMidiInput
{
    Q_OBJECT

public:
public:
    QAndroidMidiInput(const QMidiDeviceInfo &info, QAndroidJniObject usbDriver);
    virtual ~QAndroidMidiInput();

    QMidi::Error error() const override {return QMidi::NoError;}
    QMidi::State state() const override {return mState;}


private:
    QAndroidJniObject mUsbDriver;
    QMidi::State mState = QMidi::DisconnectedState;
};

QT_END_NAMESPACE

#endif // QANDROIDMIDIINPUT_H

TARGET = qtalsa_midi

QT += midi-private

LIBS += -lasound

HEADERS += \
    qalsamidiplugin.h \
    qalsamidideviceinfo.h \
    qalsamidiinput.h \
    qalsamidioutput.h \
    qalsamidishared.h \
    qalsamidiinbackend.h \
    qalsamidibackend.h \
    qalsamidioutbackend.h

SOURCES += \
    qalsamidiplugin.cpp \
    qalsamidideviceinfo.cpp \
    qalsamidiinput.cpp \
    qalsamidioutput.cpp \
    qalsamidishared.cpp \
    qalsamidiinbackend.cpp \
    qalsamidibackend.cpp \
    qalsamidioutbackend.cpp

OTHER_FILES += \
    alsa.json

PLUGIN_TYPE = midi
PLUGIN_CLASS_NAME = QAlsaPlugin
load(qt_plugin)

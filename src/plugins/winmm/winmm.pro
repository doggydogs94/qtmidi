TARGET = qtwinmm_midi

QT += midi-private

LIBS += -lWinmm

HEADERS += \
    qwinmmmidiplugin.h \
    qwinmmmidiinput.h \
    qwinmmmidioutput.h \
    qwinmmmidiinputbackend.h \
    qwinmmmidioutputbackend.h \
    qwinmmmidibackend.h

SOURCES += \
    qwinmmmidiplugin.cpp \
    qwinmmmidiinput.cpp \
    qwinmmmidioutput.cpp \
    qwinmmmidiinputbackend.cpp \
    qwinmmmidioutputbackend.cpp \
    qwinmmmidibackend.cpp

OTHER_FILES += \
   winmm.json

PLUGIN_TYPE = midi
PLUGIN_CLASS_NAME = QWinMMPlugin
load(qt_plugin)

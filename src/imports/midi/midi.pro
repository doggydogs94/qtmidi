CXX_MODULE = midi
TARGET = declarative_midi
TARGETPATH = QtMidi

QT += qml quick midi-private

HEADERS += \
    qdeclarativemidiglobal_p.h \
    qdeclarativemididevice_p.h \
    qdeclarativemidiinputdevice_p.h \
    qdeclarativemidioutputdevice_p.h \
    qdeclarativemidimessagecreator.h

SOURCES += \
    midi.cpp \
    qdeclarativemidiglobal.cpp \
    qdeclarativemididevice.cpp \
    qdeclarativemidiinputdevice.cpp \
    qdeclarativemidioutputdevice.cpp \
    qdeclarativemidimessagecreator.cpp

OTHER_FILES += \
    plugins.qmltypes

QML_FILES += \
    MidiInputComboBox.qml \
    MidiOutputComboBox.qml \
    MidiPowerBar.qml

DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0

load(qml_plugin)

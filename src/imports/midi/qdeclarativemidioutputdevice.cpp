#include "qdeclarativemidioutputdevice_p.h"
#include "qmidioutput.h"
#include "qmidiautoconnector.h"

QT_BEGIN_NAMESPACE

QDeclarativeMidiOutputDevice::QDeclarativeMidiOutputDevice(QObject *parent)
    : QDeclarativeMidiDevice(MidiOutput, parent) {

}

void QDeclarativeMidiOutputDevice::sendMessage(const QMidiMessage &message) {
    const QMidiOutput *device = m_autoConnector->device<QMidiOutput>();
    if (device) {
        device->handleMidiMessage(message);
    }
}
QT_END_NAMESPACE

import os
from subprocess import call, check_call
import argparse
import re
import platform

parser = argparse.ArgumentParser(description="Build and install for all Qt version found in QT_HOME (flag -qt)")
parser.add_argument("-j", type=int, help="Number of cores")
parser.add_argument("--qt", type=str, help="Path to QT_HOME")
parser.add_argument("--include", type=str, help="Include only specs containting the string")
parser.add_argument("--exclude", type=str, help="Exclude specs that contain the string")
parser.add_argument('--clean', action='store_true', default=False)
parser.add_argument('--vcvarsall', type=str, help="Path to vcvarsall on windows visual studio", default="")

args = parser.parse_args()

def check_env_existing(env):
    try:
        if not os.path.isdir(os.environ[env]):
            raise Exception()
    except:
        raise Exception("Environment variable %s does not exist or points to an invalid path" % env)
        
def which(program):
    import os
    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            path = path.strip('"')
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file

    return None
    
def check_program_existing(program):
    prog = which(program)
    if prog is None:
        raise Exception("Program %s not found in PATH" % program)
    else:
        print("Found %s at %s" % (program, prog))

version_re = re.compile("\d\.\d")

wd = os.getcwd()

qthome = args.qt
if qthome is None:
    if platform.system().lower() == "windows":
        qthome = "C:\\"
    else:
        qthome = os.path.expanduser("~")

    qthome = os.path.join(qthome, "Qt")

print("Searching for installed Qt Version in %s" % qthome)
if qthome is None or not os.path.isdir(qthome):
    raise Exception("QtHome dir does not exist. Use the --qt flag")


make = "make"
qmake = "qmake"
javac = "javac"
if platform.system().lower() == "windows":
    make = os.path.join(qthome, "Tools", "QtCreator", "bin", "jom")
    qmake = "qmake.exe"
    javac = "javac.exe"
    try:
        #check_program_existing("nmake.exe")
        pass
    except Exception as e:
        print("Launch the build script from a visual studio command line")
        raise e
    print("Using jom on windows")
   
    def setup_func(qt_version_str, builddir):
        print("Determine target and host arch based on %s" % qt_version_str)
        if "mingw" in qt_version_str:
            print("Nothing to setup for mingw")
            return []
            
        prog_call = []
        
        if len(args.vcvarsall) > 0:
            vcvarsall = args.vcvarsall
        elif "msvc2013" in qt_version_str:
            vcvarsall = "C:/Program Files (x86)/Microsoft Visual Studio 12.0/VC/vcvarsall.bat"
        elif "msvc2015" in qt_version_str:
            vcvarsall = "C:/Program Files (x86)/Microsoft Visual Studio 14.0/VC/vcvarsall.bat"
        elif "msvc2017" in qt_version_str:
            vcvarsall = "C:/Program Files (x86)/Microsoft Visual Studio/2017/Community/VC/Auxiliary/Build/vcvarsall.bat"
        else:
            raise Exception("No vcvarsall detected for %s" % qt_version_str)
        
        check_program_existing("cmd.exe")
        prog_call += ["cmd.exe", "/c"]
        prog_call.append("cd")
        prog_call.append(os.path.dirname(vcvarsall))
        prog_call.append("&")
        prog_call.append(os.path.basename(vcvarsall))
            
        if not os.path.isfile(vcvarsall):
            raise Exception("File %s not found" % vcvarsall)
        if platform.machine() == "i386":
            host = "x86"
        else:
            host = "amd64"
            
        if "arm" in qt_version_str:
            target = "arm"
        elif "86" in qt_version_str:
            target = "x86"
        elif "64" in qt_version_str:
            target = "amd64"
        else:
            target = "x86"

        if target == host:
            specs = target
        else:
            specs = host + "_" + target
            
        prog_call.append(specs)
            
        if "winrt" in qt_version_str:
            uwp = "store"
            prog_call.append(uwp)
        else:
            uwp = ""      
            
        print("Setting up platform for: ", vcvarsall, specs, uwp)
        
        if not os.path.isdir(os.path.dirname(vcvarsall)):
            raise Exception("not found")
            
        prog_call.append("&")
        prog_call.append("cd")
        prog_call.append(builddir)
        prog_call.append("&")
            
        # this is a bit hacky to setup the correct env for qt:
        # basically this will setup the env and run qmake in the
        # same command
        return prog_call
    
    def release_func():
        pass
            
else:
    def setup_func(qt_version_str, builddir):
        return []
        
    def release_func():
        pass
        

# find mingw version
mingw_re = re.compile("mingw\d\d\d_\d\d")
if os.path.exists(os.path.join(qthome, "Tools")):
    for tool in sorted(os.listdir(os.path.join(qthome, "Tools"))):
        if mingw_re.match(tool):
            print("Found tool %s" % tool)
            os.environ["PATH"] = os.path.join(qthome, "Tools", tool, "bin") + os.pathsep + os.environ["PATH"]

for qt_version in os.listdir(qthome):
    if version_re.match(qt_version):
        print("* Found version %s" % qt_version)
        qt_version_dir = os.path.join(qthome, qt_version)
        for qt_specs in os.listdir(qt_version_dir):
            print("* Running specs %s" % qt_specs)
            # check for includes and excludes
            if args.include and args.include not in qt_specs:
                print("* Skipping specs, does not contain %s" % args.include)
                continue
            elif args.exclude and args.exclude in qt_specs:
                print("* Skipping specs, does contain %s" % args.exclude)
                continue
                
                
            if "android" in qt_specs:
                # check if ANDROID_SDK_ROOT and ANDROID_NDK_ROOT are set
                check_env_existing("ANDROID_NDK_ROOT")
                check_env_existing("ANDROID_SDK_ROOT")
                check_program_existing(javac)
            
            qt_spec_dir = os.path.join(qt_version_dir, qt_specs)
            qt_qmake = os.path.join(qt_spec_dir, "bin", qmake)
            if os.path.isfile(qt_qmake): 
                old_path = os.environ["PATH"]
                
                os.environ["PATH"] = os.path.join(qt_spec_dir, "bin") + os.pathsep + os.environ["PATH"]
                print("  - Found spec %s" % qt_specs)
                builddir = os.path.join(wd, "build_" + qt_version + "_" + qt_specs)
                # the run env prefix can be used to run 
                run_env_prefix = setup_func(qt_specs, builddir)
                if not os.path.exists(builddir):
                    print("    Creating build dir at %s" % builddir)
                    os.makedirs(builddir)
                os.chdir(builddir)
                print("    Running qmake")
                call(run_env_prefix + [qt_qmake, "-r", ".."])
                if args.clean:
                    print("    Running make clean")
                    call([make, "clean"])
                
                print("    Running make")
                if args.j:
                    call(run_env_prefix + [make, "-j%d" % args.j])
                else:
                    call(run_env_prefix + [make])
                    
                print("    Checking if dll are created successfully")
                if len(list(os.listdir(os.path.join(builddir, "lib")))) == 0:
                    raise Exception("Unsuccessful build for %s" % qt_specs)
                    
                print("    Running make install")
                call(run_env_prefix + [make, "install"])
                      
                release_func()
                os.environ["PATH"] = old_path       
                
            else:
                print("  qmake not found at %s" % qt_qmake)



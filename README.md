# About

QtMidi is an unofficial Qt plugin that enables platform independent support (Windows, Windows Universal, Mac OS X, Linux, Android, iOS) for connecting to midi devices.

# Building

## Usage of the python build script
The reommended way to build and install qtmidi for all of your installed qt versions is to use the `build.py` script conatined in this repository.
For best usage install Qt to its default location and run
```
python build.py
```

This command will crawl all installed Qt versions and build and install QtMidi.

### General usage
Running `python build.py --help` will show you a list of flags how you can costumize the build script for your installation.
The most important features are to `--include` or `--exclude` certain Qt versions and to set the number of compilation threads, e.g. using `-j4` for 4 threads.

### Android
To build for android you need to set `ANDROID_SDK_PATH` and `ANDROID_NDK_PATH` as environment variable to your sdk/ndk installation.

## Building with QtCreator
To use this plugin in other projects you need to install it into the Qt5 repository.
For an automatic install you can simply add a `make install` build step in the QtCreator.


# Using QtMidi in other projects

Since QtMidi is installed directly as Qt plugin into its installation directory you simply need to add `midi` as a Qt module in the project file: `Qt += midi`.


## Deployment

To deploy your application based on qtmidi you need to manually copy the plugin because the deployment tools do not include the qtmidi plugin located in the midi dir.
Either you can copy the plugin by hand or add a `QMAKE_POST_LINK` that will do this automatically (see example for WinRT below).

### WinRT
The WinRT Plugin `qtwinrt_midi.dll` will not get deployed by the `windeployqt` tool. You have to provide the library in a `midi` plugin folder on your own. For example you can use the following code in your projects `pro` file:
```
winrt {
    CONFIG(debug, debug|release) {
        PLUGIN_DIR = $$OUT_PWD/debug
        PLUGIN_FILE=qtwinrt_midid.dll
    }
    else {
        PLUGIN_DIR = $$OUT_PWD/release
        PLUGIN_FILE=qtwinrt_midi.dll
    }

    PLUGIN_DIR ~= s,/,\\,g


    QMAKE_POST_LINK += \
        $$sprintf($$QMAKE_MKDIR_CMD, $$quote($$PLUGIN_DIR\\midi)) $$escape_expand(\\n\\t) \
        $$QMAKE_COPY $$quote($$(QTDIR)\\plugins\\midi\\$$PLUGIN_FILE) $$quote($$PLUGIN_DIR\\midi\\$$PLUGIN_FILE) $$escape_expand(\\n\\t)
}
```

To enable output to the default Microsoft GS Wavetable Synthesizer you need to add the following line to the `AppxManifest.xml` in the `<Dependencies>` section:
```
<PackageDependency Name="Microsoft.Midi.GmDls" MinVersion="1.0.0.0" Publisher="CN=Microsoft Corporation, O=Microsoft Corporation, L=Redmond, S=Washington, C=US" />
```

# Examples
To compile and launch the examples you need to add `QT_BUILD_PARTS+=examples` to the qmake arguments.
If you forget this step you will get an 'Application not found' exception when you try to launch an example.